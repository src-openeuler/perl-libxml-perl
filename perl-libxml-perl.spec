Name:           perl-libxml-perl
Version:        0.08
Release:        37
Summary:        Perl modules and scripts for working with XML
License:        (GPL-1.0-or-later OR Artistic-1.0-Perl) and Public Domain
URL:            https://metacpan.org/release/libxml-perl
Source0:        https://cpan.metacpan.org/authors/id/K/KM/KMACLEOD/libxml-perl-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  make perl-interpreter perl-generators perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(:VERSION) >= 5.5.0 perl(Carp) perl(IO::File) perl(strict)
BuildRequires:  perl(UNIVERSAL) perl(vars) perl(XML::Parser) >= 2.19

%global __provides_exclude ^perl\\(Data::Grove\\)$

%description
perl-libxml-perl is a collection of smaller Perl modules, scripts, and
documents for working with XML in Perl. It works in combination with
XML::Parser, PerlSAX, XML::DOM, XML::Grove and others.

%package        help
Summary:        Documents for perl-libxml-perl
BuildArch:      noarch

%description    help
perl-libxml-perl-help package contains related documents.

%prep
%autosetup -n libxml-perl-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
chmod -R u+w %{buildroot}/*

%check
make test

%files
%doc ChangeLog Changes README
%{perl_vendorlib}/Data/
%{perl_vendorlib}/XML/

%files          help
%{_mandir}/man3/*.3*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.08-37
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Nov 28 2019 Jiangping Hu <hujiangping@huawei.com> - 0.08-36
- package init
